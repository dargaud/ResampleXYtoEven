// Resample unevenly distributed X/Y data into evenly spaced X
// (so you can use FFT, filters, etc)
// Author: Guillaume Dargaud
// Compile with /DTEST_RXY2E for test program

#include <stdlib.h>
#include "ResampleXYtoEven.h"

/// HIFN	Interpolate y based on position of x between X0,Y0 and X1,Y1
static __inline__ double InterpolY(double X0, double X1,
								  double Y0, double Y1, double x) {
	return Y0+(Y1-Y0)*(x-X0)/(X1-X0);
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Function to resample X/Y data into evenly spaced X
/// HIPAR	ResamplingDX / The resampling value along the the X axis
/// HIPAR	ResamplingDX / Ideally should be the smallest X[i+1]-X[i]. Or maybe a bit less ?
/// HIPAR	X / Unevenly distributed positions to resample. GROWING: x[i]<x[i+1]
/// HIPAR	Y / Corresponding values
/// HIPAR	NbXY / Number of values in X and Y
/// HIPAR	ResamplingDX / The interval you want for the resampling.
/// HIPAR	ResamplingDX / Should be at most MIN(X[i+1]-X[i]) for any i
/// HIPAR	RsY / Re-Interpolated Y values (allocated inside, you must free).
/// HIPAR	RsY / Pass NULL to only obtain the number of resamples
/// HIPAR	SumRsY / Optional sum of all RsY values (used for further processing, pass NULL if not needed)
/// HIRET	Number of items in returned resampled array
///////////////////////////////////////////////////////////////////////////////
int ResampleXYtoEven(double X[], double Y[], int NbXY, double ResamplingDX,
							   double** RsY, double *SumRsY) {
	int idx=0, NbRsmpl=0, i;
	double SS=0, x;
	NbRsmpl=(int)((X[NbXY-1]-X[0])/ResamplingDX+1);	// +1 somewhere
	if (RsY==NULL) return NbRsmpl;

	// Now let's copy data for real
	*RsY = realloc(*RsY, NbRsmpl*sizeof(double));
	idx=0;

	for (i=0; i<NbRsmpl; i++) {
		if ((x=(X[0] + i*ResamplingDX)) >= X[idx+1]) {
			if (++idx>=NbXY-1) {
				if (i==NbRsmpl-1) SS += (*RsY)[i] = Y[NbXY-1];	// Last value
				break;
			}
		}
		SS += (*RsY)[i] = InterpolY(X[idx], X[idx+1], Y[idx], Y[idx+1], x);
	}

	// TODO: antialiasing filter

	if (SumRsY!=NULL) *SumRsY=SS;
	return NbRsmpl;
}

///////////////////////////////////////////////////////////////////////////////
#ifdef TEST_RXY2E

#include <stdio.h>
#include <cvirte.h>
#include <userint.h>

int CVICALLBACK cbp_Close(int panel, int event, void *callbackData,
						   int eventData1, int eventData2) {
	switch (event) {
		case EVENT_CLOSE:
			QuitUserInterface (0);
			break;
	}
	return 0;
}

int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;    /* out of memory */

	int Pnl =NewPanel(0, "Easy Resampling", 30, 30, 600, 800);
	int Grf =NewCtrl (Pnl, CTRL_GRAPH_LS, "", 0, 0);
	SetCtrlAttribute (Pnl, Grf, ATTR_HEIGHT, 600);
	SetCtrlAttribute (Pnl, Grf, ATTR_WIDTH,  800);
	SetCtrlAttribute (Pnl, Grf, ATTR_ENABLE_ZOOM_AND_PAN, 1);
	SetPanelAttribute(Pnl, ATTR_CALLBACK_FUNCTION_POINTER, cbp_Close);
	DisplayPanel(Pnl);

	// Uneven data, but falls on perfect X positions
	double X1[]={3,10,11,12,15,20,30,31,50,51},
		   Y[] ={3,10,11,11,11, 5, 6,20,19,1 },
			*RsY=NULL, Rsmpl;
	int NXY=sizeof(X1)/sizeof(double), Nb;

	Nb=ResampleXYtoEven(X1, Y, 10, Rsmpl=1., &RsY, NULL);
//	for (int i=0; i<Nb; i++) printf("%.1f\t%.1f\n", i*Rsmpl, RsY[i]);
	PlotXY (Pnl, Grf, X1, Y, NXY, VAL_DOUBLE, VAL_DOUBLE,
			VAL_CONNECTED_POINTS, VAL_SOLID_SQUARE,	VAL_SOLID, 1, VAL_YELLOW);
	PlotWaveform (Pnl, Grf, RsY, Nb, VAL_DOUBLE, 1.0, 0.0, X1[0], Rsmpl,
				  VAL_CONNECTED_POINTS, VAL_SOLID_DIAMOND, VAL_SOLID, 1, VAL_RED);

	RunUserInterface(); DeleteGraphPlot (Pnl, Grf, -1, VAL_IMMEDIATE_DRAW);

	SetPanelAttribute (Pnl, ATTR_TITLE, "Now all points are a bit off");
	double X2[]={3.5,10.1,11.1,12.2,15.9,20.1,30.5,31.6,50.2,51};
	Nb=ResampleXYtoEven(X2, Y, 10, Rsmpl=1., &RsY, NULL);
	PlotXY (Pnl, Grf, X2, Y, NXY, VAL_DOUBLE, VAL_DOUBLE,
			VAL_CONNECTED_POINTS, VAL_SOLID_SQUARE, VAL_SOLID, 1, VAL_YELLOW);
	PlotWaveform (Pnl, Grf, RsY, Nb, VAL_DOUBLE, 1.0, 0.0, X2[0], Rsmpl,
				  VAL_CONNECTED_POINTS, VAL_SOLID_DIAMOND, VAL_SOLID, 1, VAL_RED);

	RunUserInterface(); DeleteGraphPlot (Pnl, Grf, -1, VAL_IMMEDIATE_DRAW);

	SetPanelAttribute (Pnl, ATTR_TITLE, "Same with higher resampling");
	Nb=ResampleXYtoEven(X2, Y, 10, Rsmpl=.1, &RsY, NULL);
	PlotXY (Pnl, Grf, X2, Y, NXY, VAL_DOUBLE, VAL_DOUBLE,
			VAL_CONNECTED_POINTS, VAL_SOLID_SQUARE,	VAL_SOLID, 1, VAL_YELLOW);
	PlotWaveform (Pnl, Grf, RsY, Nb, VAL_DOUBLE, 1.0, 0.0, X2[0], Rsmpl,
				  VAL_CONNECTED_POINTS, VAL_SOLID_DIAMOND, VAL_SOLID, 1, VAL_RED);

	RunUserInterface(); DeleteGraphPlot (Pnl, Grf, -1, VAL_IMMEDIATE_DRAW);

	SetPanelAttribute (Pnl, ATTR_TITLE, "Now insufficient resampling (don't do that !)");
	Nb=ResampleXYtoEven(X2, Y, 10, Rsmpl=1.5, &RsY, NULL);
	PlotXY (Pnl, Grf, X2, Y, NXY, VAL_DOUBLE, VAL_DOUBLE,
			VAL_CONNECTED_POINTS, VAL_SOLID_SQUARE,	VAL_SOLID, 1, VAL_YELLOW);
	PlotWaveform (Pnl, Grf, RsY, Nb, VAL_DOUBLE, 1.0, 0.0, X2[0], Rsmpl,
				  VAL_CONNECTED_POINTS, VAL_SOLID_DIAMOND, VAL_SOLID, 1, VAL_RED);

	RunUserInterface(); DeleteGraphPlot (Pnl, Grf, -1, VAL_IMMEDIATE_DRAW);

	X2[0]=3; X2[NXY-1]=50.999;
	SetPanelAttribute (Pnl, ATTR_TITLE, "Variation 1 on ending");
	Nb=ResampleXYtoEven(X2, Y, 10, Rsmpl=1, &RsY, NULL);
	PlotXY (Pnl, Grf, X2, Y, NXY, VAL_DOUBLE, VAL_DOUBLE,
			VAL_CONNECTED_POINTS, VAL_SOLID_SQUARE,	VAL_SOLID, 1, VAL_YELLOW);
	PlotWaveform (Pnl, Grf, RsY, Nb, VAL_DOUBLE, 1.0, 0.0, X2[0], Rsmpl,
				  VAL_CONNECTED_POINTS, VAL_SOLID_DIAMOND, VAL_SOLID, 1, VAL_RED);

	RunUserInterface(); DeleteGraphPlot (Pnl, Grf, -1, VAL_IMMEDIATE_DRAW);

	X2[NXY-1]=51.001;
	SetPanelAttribute (Pnl, ATTR_TITLE, "Variation 2 on ending");
	Nb=ResampleXYtoEven(X2, Y, 10, Rsmpl=1, &RsY, NULL);
	PlotXY (Pnl, Grf, X2, Y, NXY, VAL_DOUBLE, VAL_DOUBLE,
			VAL_CONNECTED_POINTS, VAL_SOLID_SQUARE,	VAL_SOLID, 1, VAL_YELLOW);
	PlotWaveform (Pnl, Grf, RsY, Nb, VAL_DOUBLE, 1.0, 0.0, X2[0], Rsmpl,
				  VAL_CONNECTED_POINTS, VAL_SOLID_DIAMOND, VAL_SOLID, 1, VAL_RED);

	RunUserInterface(); DeleteGraphPlot (Pnl, Grf, -1, VAL_IMMEDIATE_DRAW);

	free(RsY);
	DiscardPanel(Pnl);
	return 0;
}
#endif