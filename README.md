# ResampleXYtoEven

(LabWindows/CVI) C function to resample unevenly distributed X/Y data into evenly spaced X (so you can use FFT, filters, etc)

The function itself is standard C, but the test program is LabWindows/CVI
