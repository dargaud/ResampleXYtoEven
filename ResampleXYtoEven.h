#ifndef __RESAMPLE_XY_2_EVEN_H
#define __RESAMPLE_XY_2_EVEN_H

extern int ResampleXYtoEven(double X[], double Y[], int NbXY, double ResamplingDX,
							   double** RsY, double *SumRsY);

#endif
